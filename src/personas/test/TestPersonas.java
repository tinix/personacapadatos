package personas.test;

import java.sql.SQLException;
import java.util.List;
import java.sql.*;
import personas.dto.PersonaDTO;
import personas.jdbc.PersonaDao;
import personas.jdbc.PersonaDaoJDBC;

public class TestPersonas {
	
	public static void main(String[] args) {
		
		PersonaDao personaDao = new PersonaDaoJDBC();
		
		PersonaDTO persona = new PersonaDTO();
		persona.setNombre("George");
		persona.setApellido("Tinix");
		try {
			PersonaDTO personaTmp = new PersonaDTO();
			List<PersonaDTO> personas = personaDao.select();
			for (PersonaDTO personaDTO : personas) {
				System.out.print(personaDTO);
				System.out.println();
			}
		} catch (SQLException e) {
			System.out.println("Excepcion en la capa de prueba");
			e.printStackTrace();
		}
	}

}
