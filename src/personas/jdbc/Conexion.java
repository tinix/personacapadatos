package personas.jdbc;

import java.sql.Connection;
import java.sql.*;
import com.mysql.cj.jdbc.Driver;

public class Conexion {
	
	private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String JDBC_URL = "jdbc:mysql://localhost:3306/sga?useSSL=false";
	private static final String JDBC_USER = "root";
	private static final String JDBC_PASS = "root";
	private static Driver driver = null;
	
	
	public static synchronized Connection getConnection() 
			throws SQLException {
		if(driver == null) {
			try {
				Class.forName(JDBC_DRIVER).newInstance();
			} catch (Exception e) {
				System.out.println("Fallo en cargarel dirver JDBC");
				e.printStackTrace();
			}
		}
		return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
	}
	
	public static void close(ResultSet rs) {
		try {
			if(rs != null) {
				rs.close();
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}
	
	public static void close(PreparedStatement stmt) {
		try {
			if(stmt != null) {
				stmt.close();
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}
	
	public static void close(Connection conn) {
		try {
			if(conn != null) {
				conn.close();
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}
	

}
