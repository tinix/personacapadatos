package personas.jdbc;


import java.util.*;
import java.sql.*;
import personas.dto.PersonaDTO;
	
	
public interface PersonaDao {
	public int insert(PersonaDTO persona) throws SQLException;
	public int update(PersonaDTO persona) throws SQLException;
	public int delete(PersonaDTO persona) throws SQLException;
	public List<PersonaDTO> select() throws SQLException;
	
}

